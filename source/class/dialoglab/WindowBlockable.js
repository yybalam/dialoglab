qx.Class.define("dialoglab.WindowBlockable", {
  extend: qx.ui.window.Window,
  include: [qx.ui.core.MBusyBlocker],

  construct: function() {
    this.base(arguments);

    this.setCaption('A blockable window');
    this.setLayout(new qx.ui.layout.Basic());

    this.add(new qx.ui.basic.Label('A simple label.'));

    this.setWidth(300);
    this.setHeight(150);
  }
});
