/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("dialoglab.theme.Theme",
{
  meta :
  {
    color : dialoglab.theme.Color,
    decoration : dialoglab.theme.Decoration,
    font : dialoglab.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : dialoglab.theme.Appearance
  }
});
