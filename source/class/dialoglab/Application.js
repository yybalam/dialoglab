/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

/**
 * This is the main application class of your custom application "dialoglab"
 *
 * @asset(dialoglab/*)
 */
qx.Class.define("dialoglab.Application",
{
  extend : qx.application.Standalone,



  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /**
     * This method contains the initial application code and gets called
     * during startup of the application
     *
     * @lint ignoreDeprecated(alert)
     */
    main : function()
    {
      // Call super class
      this.base(arguments);

      this.getRoot().setBlockerColor('black');
			this.getRoot().setBlockerOpacity(0.2);

      // Enable logging in debug variant
      if (qx.core.Environment.get("qx.debug"))
      {
        // support native logging capabilities, e.g. Firebug for Firefox
        qx.log.appender.Native;
        // support additional cross-browser console. Press F7 to toggle visibility
        qx.log.appender.Console;
      }

      // Buttons hor show dialog
      var btnAlert = new qx.ui.form.Button("Show Alert");
      var btnWarning = new qx.ui.form.Button("Show Warning");
      var btnError = new qx.ui.form.Button("Show Error");
      var btnConfirm = new qx.ui.form.Button("Show Confirm");
      var btnBlock = new qx.ui.form.Button("Show Block");
      var btnBusy = new qx.ui.form.Button("Show Busy");

      // Document is the application root
      var doc = this.getRoot();

      // Add button to document at fixed coordinates
      doc.add(btnAlert, {left: 50, top: 50});
      doc.add(btnWarning, {left: 50, top: 100});
      doc.add(btnError, {left: 50, top: 150});
      doc.add(btnConfirm, {left: 50, top: 200});
      doc.add(btnBlock, {left: 50, top: 250});
      doc.add(btnBusy, {left: 50, top: 300});

      // Add an event listener
      btnAlert.addListener("execute", function(e) {
        qx.ui.Dialog.alert(
          'This is an alert text demo.'
        );
      });

      btnWarning.addListener("execute", function(e) {
        qx.ui.Dialog.warning(
          'This is a warning text demo.'
        );
      });

      btnError.addListener("execute", function(e) {
        qx.ui.Dialog.error(
          'This is an error text demo.'
        );
      });

      btnConfirm.addListener("execute", function(e) {
        qx.ui.Dialog.confirm({
          caption: "Confirm Title",
          message: "What yo will to do cannot be undone.",
          buttons: [{
              button: "yes",
              callback: function() {
                qx.ui.Dialog.alert('This is an alert shown by confirm yes button.');
              }
            }, {
              button: "no",
              label: "¡No no!",
              callback: function() {
                qx.ui.Dialog.alert('Esto está en español. ;)');
              }
            }, {
              button: "cancel",
              callback: function() {
                qx.ui.Dialog.error('This error is shown by confirm cancel button.');
              }
            }
          ]
        }).setContext(this);
      });

      btnBlock.addListener("execute", function(e) {
        var win = new dialoglab.WindowBlockable();
        win.show();
        win.center();

        win.addListener('appear', function(){
          win.block();
        });

        qx.event.Timer.once(function() {
          win.unblock();
          win.close();
        }, this, 3000);
      });

      btnBusy.addListener("execute", function(e) {
        var win = new dialoglab.WindowBusy();
        win.show();
        win.center();

        win.addListener('appear', function(){
          win.block();
        });

        qx.event.Timer.once(function() {
          win.unblock();
          win.close();
        }, this, 3000);
      });
    }
  }
});
